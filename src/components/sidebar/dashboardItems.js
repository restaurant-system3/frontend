/* eslint-disable prettier/prettier */
import {
  Bell,
  BookOpen,
  Calendar,
  CheckSquare,
  Grid,
  Layout,
  List,
} from "react-feather";

const featuresSection = [
  {
    href: "/staff",
    icon: Bell,
    title: "Staff"
  },
  {
    href: "/table",
    icon: BookOpen,
    title: "Table"
  },
  {
    href: "/category",
    icon: Calendar,
    title: "Category"
  },
  {
    href: "/stock",
    icon: CheckSquare,
    title: "Stock"
  },
  {
    href: "/product",
    icon: Grid,
    title: "Product",
    children: [
      {
        href: "/product/food",
        title: "Food",
      },
      {
        href: "/product/drink",
        title: "Drink",
      },
    ],
  },
  {
    href: "/order",
    icon: Layout,
    title: "Order"
  },
  {
    href: "/payment-history",
    icon: List,
    title: "Payment History"
  },
];



const navItems = [
  {
    title: "",
    pages: featuresSection,
  },
];

export default navItems;
