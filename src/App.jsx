/* eslint-disable prettier/prettier */
import React, { Suspense } from "react";
import { useRoutes } from "react-router-dom";
import { HelmetProvider, Helmet } from "react-helmet-async";

import "./i18n";
import routes from "./routes";

import Loader from "./components/Loader";

import ThemeProvider from "./contexts/ThemeProvider";
import SidebarProvider from "./contexts/SidebarProvider";
import LayoutProvider from "./contexts/LayoutProvider";
import ChartJsDefaults from "./utils/ChartJsDefaults";

const App = () => {
  const content = useRoutes(routes);

  return (
    <HelmetProvider>
      <Helmet
        titleTemplate="%s"
        defaultTitle="Restaurant"
      />
      <Suspense fallback={<Loader />}>
        <ThemeProvider>
          <SidebarProvider>
            <LayoutProvider>
              <ChartJsDefaults />
              {content}
            </LayoutProvider>
          </SidebarProvider>
        </ThemeProvider>
      </Suspense>
    </HelmetProvider>
  );
};

export default App;
