/* eslint-disable prettier/prettier */
import { Helmet } from "react-helmet-async";
import { Card, Container } from "react-bootstrap";

import PaymentHistoryComponent from "../../components/paymentHistory/PaymentHistoryComponent";

function PaymentHistory() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Payment history Page</h1>
            <Card>
               <Card.Body>
                  <PaymentHistoryComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default PaymentHistory;
