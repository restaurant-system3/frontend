/* eslint-disable prettier/prettier */

import { Helmet } from "react-helmet-async";
import StockComponent from "../../components/stock/StockComponent";
import { Card, Container } from "react-bootstrap";

function StockPage() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Stock Page</h1>
            <Card>
               <Card.Body>
                  <StockComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default StockPage;
