/* eslint-disable prettier/prettier */
import { Card, Container } from "react-bootstrap";
import { Helmet } from "react-helmet-async";

import StaffComponent from "../../components/staff/StaffComponent";

function Staff() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Staff Page</h1>
            <Card>
               <Card.Body>
                  <StaffComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default Staff;
