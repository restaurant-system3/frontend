/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet-async";
import { Card, Container } from "react-bootstrap";

import CategoryTableComponent from "../../components/category/CategoryTableComponent";
import CategoryAPI from "../../api/CategoryAPI";

const CategoryPage = () => {
   const [categories, setCategories] = useState([]);

   useEffect(() => {
      getAllCategories();
   }, []);

   const getAllCategories = async () => {
      const data = await CategoryAPI.getAll();
      setCategories(data.content);
   }
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Category Page</h1>
            <Card>
               <Card.Body>
                  <CategoryTableComponent categories={categories} />
               </Card.Body>
            </Card>
         </Container>
      </>
   )
};

export default CategoryPage;
