/* eslint-disable prettier/prettier */
import { Helmet } from "react-helmet-async";
import { Card, Container } from "react-bootstrap";

import OrderComponent from "../../components/order/OrderComponent";

function OrderPage() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Order Page</h1>
            <Card>
               <Card.Body>
                  <OrderComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default OrderPage;
