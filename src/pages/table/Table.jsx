/* eslint-disable prettier/prettier */
import { Card, Container } from "react-bootstrap";
import { Helmet } from "react-helmet-async";
import TableComponent from "../../components/table/TableComponent";

function Table() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Product Page</h1>
            <Card>
               <Card.Body>
                  <TableComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default Table;
