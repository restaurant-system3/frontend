/* eslint-disable prettier/prettier */
import { Card, Container } from "react-bootstrap";
import { Helmet } from "react-helmet-async";

import ProductFoodComponent from "../../components/product/ProductFoodComponent";

function ProductFoodPage() {
   return (
      <>
         <Helmet title="Account" />
         <Container fluid className="p-0">
            <h1 className="h3 mb-3">Product Food Page</h1>
            <Card>
               <Card.Body>
                  <ProductFoodComponent />
               </Card.Body>
            </Card>
         </Container>
      </>
   );
}
export default ProductFoodPage;
