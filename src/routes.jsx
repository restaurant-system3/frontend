/* eslint-disable prettier/prettier */
/* eslint-disable import/first */
import React from "react";
import { lazy } from "@loadable/component";

// Layouts
import AuthLayout from "./layouts/Auth";
import DashboardLayout from "./layouts/Dashboard";

//HomePage
const HomePage = lazy(() => import("./pages/home/Home"));

//Staff
const StaffPage = lazy(() => import("./pages/staff/Staff.jsx"));

//Table
const TablePage = lazy(() => import("./pages/table/Table.jsx"));

//Category
const CategoryPage = lazy(() => import("./pages/category/Category.jsx"));

//Stock
const StockPage = lazy(() => import("./pages/stock/Stock.jsx"));

//Product
const ProductFoodPage = lazy(() => import("./pages/product/ProductFood.jsx"));
const ProductDrinkPage = lazy(() => import("./pages/product/ProductDrink.jsx"));

//Order
const OrderPage = lazy(() => import("./pages/order/Order.jsx"));

//Payment history
const PaymentHistoryPage = lazy(() => import("./pages/paymentHistory/PaymentHistory.jsx"));

// Auth
const Page500 = lazy(() => import("./pages/auth/Page500"));
const Page404 = lazy(() => import("./pages/auth/Page404"));
const SignIn = lazy(() => import("./pages/auth/SignIn"));
const SignUp = lazy(() => import("./pages/auth/SignUp"));
const ResetPassword = lazy(() => import("./pages/auth/ResetPassword"));

// Icons
const Feather = lazy(() => import("./pages/icons/Feather"));
const FontAwesome = lazy(() => import("./pages/icons/FontAwesome"));

const routes = [
  {
    path: "/",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <HomePage />,
      },
    ],
  },
  {
    path: "staff",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <StaffPage />,
      },
    ],
  },
  {
    path: "table",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <TablePage />,
      },
    ],
  },
  {
    path: "category",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <CategoryPage />,
      },
    ],
  },
  {
    path: "stock",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <StockPage />,
      },
    ],
  },
  {
    path: "product",
    element: <DashboardLayout />,
    children: [
      {
        path: "food",
        element: <ProductFoodPage />,
      },
      {
        path: "drink",
        element: <ProductDrinkPage />,
      },
    ],
  },
  {
    path: "order",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <OrderPage />,
      },
    ],
  },
  {
    path: "payment-history",
    element: <DashboardLayout />,
    children: [
      {
        path: "",
        element: <PaymentHistoryPage />,
      },
    ],
  },
  {
    path: "auth",
    element: <AuthLayout />,
    children: [
      {
        path: "sign-in",
        element: <SignIn />,
      },
      {
        path: "sign-up",
        element: <SignUp />,
      },
      {
        path: "reset-password",
        element: <ResetPassword />,
      },
      {
        path: "404",
        element: <Page404 />,
      },
      {
        path: "500",
        element: <Page500 />,
      },
    ],
  },
  {
    path: "icons",
    element: <DashboardLayout />,
    children: [
      {
        path: "feather",
        element: <Feather />,
      },
      {
        path: "font-awesome",
        element: <FontAwesome />,
      },
    ],
  },
  {
    path: "*",
    element: <AuthLayout />,
    children: [
      {
        path: "*",
        element: <Page404 />,
      },
    ],
  },
];

export default routes;
